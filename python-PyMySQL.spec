%global _empty_manifest_terminate_build 0
Name:		python-PyMySQL
Version:	1.1.1
Release:	1
Summary:	Pure Python MySQL Client
License:	MIT
URL:		https://pypi.python.org/pypi/PyMySQL/
Source0:	https://files.pythonhosted.org/packages/41/9d/ee68dee1c8821c839bb31e6e5f40e61035a5278f7c1307dde758f0c90452/PyMySQL-1.1.1.tar.gz
BuildArch:	noarch

%description
This package contains a pure-Python MySQL client library, based on PEP 249.
Most public APIs are compatible with mysqlclient and MySQLdb.

%package -n python3-PyMySQL
Summary:	Pure Python MySQL Client
Provides:	python-PyMySQL = %{version}-%{release}
BuildRequires:	python3-cryptography
BuildRequires:	python3-devel
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
Requires:	python3-cryptography
%description -n python3-PyMySQL
This package contains a pure-Python MySQL client library, based on PEP 249.
Most public APIs are compatible with mysqlclient and MySQLdb.

%package help
Summary:	Development documents and examples for pymysql
Provides:	python3-PyMySQL-doc
%description help
This package contains a pure-Python MySQL client library, based on PEP 249.
Most public APIs are compatible with mysqlclient and MySQLdb.

%prep
%autosetup -n PyMySQL-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-PyMySQL
%{python3_sitelib}/*

%files help
%{_docdir}/*

%changelog
* Mon Jun 03 2024 liuzhilin <liuzhilin@kylinos.cn> - 1.1.1-1
- Update package to version 1.1.1
- Prohibit dict parameter for Cursor.execute(). It didn't produce valid SQL and might cause SQL injection. (CVE-2024-36039)
- Added ssl_key_password param.

* Thu May 23 2024 yinyongkang <yinyongkang@kylinos.cn> - 1.1.0-2
- Fix CVE-2024-36039

* Tue Jul 04 2023 chenzixuan <chenzixuan@kylinos.cn> - 1.1.0-1
- Update package to version 1.1.0

* Sat May 6 2023 Ge Wang <wang__ge@126.com> - 1.0.3-2
- Replace compilation method form setuptools to pyproject

* Thu Apr 6 2023 wubijie <wubijie@kylinos.cn> - 1.0.3-1
- Update package to version 1.0.3

* Tue Nov 22 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0.2-1
- Update package to version 1.0.2

* Wed Aug 25 2021 OpenStack_SIG <openstack@openeuler.org> - 0.9.3-3
- Revert the version to 0.9.3, because python3-aiomysql depends on the python-PyMySQL vertion ranging form 0.9 to 0.9.3

* Mon Aug 09 2021 OpenStack_SIG <openstack@openeuler.org> - 1.0.2-1
- Update version to 1.0.2, because of the dependence of Openstack Wallaby

* Fri Oct 30 2020 yanglongkang <yanglongkang@huawei.com> - 0.9.3-2
- remove python2 dependency

* Thu Jul 16 2020 yanglongkang <yanglongkang@huawei.com> - 0.9.3-1
- update package to 0.9.3

* Fri Feb 14 2020 hy-euler <eulerstoragemt@huawei.com> - 0.9.2-3
- Package Initialization

